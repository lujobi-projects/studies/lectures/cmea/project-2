
#include "forward_euler.hpp"
#include "create_poisson_matrix.hpp"
#include <iostream>

//! Uses the explicit forward Euler method to compute u from time 0 to time T
//!
//! @param[in] u0 the initial data, as column vector (size N+2)
//! @param[in] dt the time step size
//! @param[in] T the final time at which to compute the solution (which we assume to be a multiple of dt)
//! @param[in] N the number of interior grid points
//! @param[in] gL function of time with the Dirichlet condition at left boundary
//! @param[in] gR function of time with the Dirichlet condition at right boundary
//! @param[in] a the coefficient function a
//!
//! @return u at all time steps up to time T, each column corresponding to a time step (including the initial condition as first column)
//!
//! @note the vector returned should include the boundary values!
std::pair<Eigen::MatrixXd, Eigen::VectorXd> forwardEuler(
    const Eigen::VectorXd& u0,
    double dt,
    double T,
    int N,
    const std::function<double(double)>& gL,
    const std::function<double(double)>& gR,
    const std::function<double(double)>& a) {


    const int nsteps = int(round(T / dt));

    const double h = 1. / (N + 1);

    Eigen::MatrixXd u;
    u.resize(N + 2, nsteps + 1);
    u.setZero();

    Eigen::VectorXd time;
    time.resize(nsteps + 1);

    for(int i = 0; i < N+2; i++){
        u(i, 0) = u0(i);
    }

    for (int t = 1;t <= nsteps; t++){
        u(0, t) = gL(t*dt);
        u(N+1, t) = gR(t*dt);
    }

    SparseMatrix ident;
    ident.resize(N, N);
    ident.setIdentity();
    SparseMatrix A = ident + createPoissonMatrix(N, a)*h;

    Eigen::SparseLU<SparseMatrix> solver;
    solver.compute(A);

    if ( solver.info() !=  Eigen::Success) {
        throw std::runtime_error("Could not decompose the matrix");
    }

    time(0) = 0;

    /*std::cout << u << std::endl << std::endl;

    Eigen::Vector3d x;
    x << 0.0625, 0, 0.0625;
    std::cout << Eigen::MatrixXd(A) << std::endl << std::endl;
    std::cout << A*u.col(0).segment(0,N) << std::endl << std::endl;
    */

    for (int j = 1; j <= nsteps; ++j) {
        Eigen::VectorXd rhs = u.col(j-1).segment(1, N);
        rhs(0) += u(0,j)/h;
        rhs(N-1) += u(N+1,j)/h;

        u.col(j).segment(1, N) = solver.solve(rhs);

        /*std::cout << rhs << std::endl << std::endl;
        std::cout << u << std::endl << std::endl;
        */
        time(j) = dt*j;
    }

    return std::make_pair(u, time);
}

