#include "create_poisson_matrix.hpp"

//! Used for filling the sparse matrix.
using Triplet = Eigen::Triplet<double>;

//! Create the 1D Poisson matrix
//! @param[in] N the number of interior points
//! @param[in] a the coefficient function a
//!
//! @returns the Poisson matrix.
SparseMatrix createPoissonMatrix(int N,
    const std::function<double(double)>& a) {

    SparseMatrix A;

    A.resize(N, N);

    std::vector<Triplet> triplets;
    triplets.reserve(N + 2 * N - 2);
    const double h = 1./(N+1);

    for (int i = 1; i < N-1; ++i) {
        // This is the diagonal
        const double lambda = a((i+1)*h);
        triplets.push_back(Triplet(i, i, 2*lambda));
        triplets.push_back(Triplet(i, i-1, -lambda));
        triplets.push_back(Triplet(i, i+1, -lambda));
    }

    // Deal with first and last row here
    triplets.push_back(Triplet(0, 0, 2*a(1*h)));
    triplets.push_back(Triplet(0, 1, -a(1*h)));
    triplets.push_back(Triplet(N-1, N-1, 2*a(N*h)));
    triplets.push_back(Triplet(N-1, N-2, -a(N*h)));

    A.setFromTriplets(triplets.begin(), triplets.end());

    A/=h*h;

    return A;
}

