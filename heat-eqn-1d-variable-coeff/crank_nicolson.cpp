#include "crank_nicolson.hpp"


//! Uses the Crank-Nicolson method to compute u from time 0 to time T
//!
//! @param[in] u0 the initial data, as column vector (size N+2)
//! @param[in] dt the time step size
//! @param[in] T the final time at which to compute the solution (which we assume to be a multiple of dt)
//! @param[in] N the number of interior grid points
//! @param[in] gL function of time with the Dirichlet condition at left boundary
//! @param[in] gR function of time with the Dirichlet condition at right boundary
//! @param[in] a the coefficient function a
//!
//! @note the vector returned should include the boundary values!
//!
std::pair<Eigen::MatrixXd, Eigen::VectorXd> crankNicolson(
    const Eigen::VectorXd& u0,
    double dt, double T, int N,
    const std::function<double(double)>& gL,
    const std::function<double(double)>& gR,
    const std::function<double(double)>& a) {

    Eigen::VectorXd time;
    Eigen::MatrixXd u;

    const int nsteps = int(round(T / dt));

    const double h = 1. / (N + 1);

    u.resize(N + 2, nsteps + 1);
    u.setZero();

    time.resize(nsteps + 1);

    for(int i = 0; i < N+2; i++){
        u(i, 0) = u0(i);
    }

    for (int t = 1;t <= nsteps; t++){
        u(0, t) = gL(t*dt);
        u(N+1, t) = gR(t*dt);
    }

    SparseMatrix ident;
    ident.resize(N, N);
    ident.setIdentity();
    SparseMatrix A = ident +h*h*createPoissonMatrix(N, a)/2;

    Eigen::SparseLU<SparseMatrix> solver;
    solver.compute(A);

    if ( solver.info() !=  Eigen::Success) {
        throw std::runtime_error("Could not decompose the matrix");
    }

    time(0) = 0;

    //std::cout << A*u.col(0).segment(1,3) << std::endl << std::endl;

    for (int j = 1; j <= nsteps; ++j) {
        Eigen::VectorXd rhs = u.col(j-1).segment(1, N);
        rhs(0) += u(0,j);
        rhs(N-1) += u(N+1,j);

        u.col(j).segment(1, N) = solver.solve(rhs);

        time(j) = dt*j;
    }

    return std::make_pair(u, time);
}
