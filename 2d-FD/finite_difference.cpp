#include <Eigen/Sparse>
#include <iostream>
#include "writer.hpp"
#include <cmath>
#include <Eigen/SparseCholesky>
#include <stdexcept>

//! Sparse Matrix type. Makes using this type easier.
typedef Eigen::SparseMatrix<double> SparseMatrix;

//! Used for filling the sparse matrix.
typedef Eigen::Triplet<double> Triplet;


//! Vector type
typedef Eigen::VectorXd Vector;


//! Our function pointer, typedef'd to make it easier to use
typedef double(*FunctionPointer)(double, double);

//----------------poissonBegin----------------
//! Create the Poisson matrix for 2D finite difference.
//! @param[out] A will be the Poisson matrix (as in the exercise)
//! @param[in] N number of elements in the x-direction
//! @param[in] dx the cell width
void createPoissonMatrix2D(SparseMatrix& A, int N, double dx) {
     // Fill the matrix A using setFromTriplets - method 
    // (see exercise 1 for how to use it).

    //TODO Why do i have dx here?!?

    A.resize(N*N, N*N);

    std::vector<Triplet> triplets;
    triplets.reserve(5*N*N - 2*N - 2);

    for (int i = 0; i < N*N; ++i) {
        // This is the diagonal
        triplets.push_back(Triplet(i, i, 4));
    }

    for (int i = 0; i < N*N-1; ++i) {
        triplets.push_back(Triplet(i, i + 1, -1));
        triplets.push_back(Triplet(i + 1, i, -1));
    }

    for (int i = 0; i < N*N - N; ++i) {
        triplets.push_back(Triplet(i, i + N, -1));
        triplets.push_back(Triplet(i + N, i, -1));
    }

    A.setFromTriplets(triplets.begin(), triplets.end());
}
//----------------poissonEnd----------------


//----------------RHSBegin----------------
//! Create the Right hand side for the 2D finite difference
//! @param[out] rhs will at the end contain the right hand side
//! @param[in] f the right hand side function f
//! @param[in] N the number of points in the x direction
//! @param[in] dx the cell width
//! @param[in] g the boundary condition function g
void createRHS(Vector& rhs, FunctionPointer f, int N, double dx, FunctionPointer g) {
    rhs.resize(N * N);
    // fill up RHS
    // remember that the index (i,j) corresponds to j*N+i
    for(int i = 0; i < N; i++)
        for(int j = 0; j < N; j++)
            rhs[i*N+j] = f(i*dx, j*dx);

    rhs *= dx*dx;

    //specially consider the boundary
    for (int k = 0; k < N; ++k) {
        //bottom
        rhs[k] += g((k+1)*dx, 0);
        //top
        rhs[k + N*(N-1)] += g((k+1)*dx, 1);
        //left
        rhs[k*N] += g(0, (k+1)*dx);
        //right
        rhs[k*N+(N-1)] += g(1, (k+1)*dx);
    }
}
//----------------RHSEnd----------------


//----------------solveBegin----------------
//! Solve the Poisson equation in 2D
//! @param[out] u will contain the solution u
//! @param[in] f the function pointer to f
//! @param[in] N the number of points to use (in x direction)
void poissonSolve(Vector& u, FunctionPointer f, int N, FunctionPointer g) {
    // Solve Poisson 2D here
    double dx = 1.0 / (N + 1);

    SparseMatrix A;
    // create the matrix
    createPoissonMatrix2D(A, N, dx);

    Vector rhs;
    // create RHS
    createRHS(rhs, f, N, dx, g);

    Eigen::SparseLU<SparseMatrix> solver;

    solver.compute(A);

    if ( solver.info() !=  Eigen::Success) {
        throw std::runtime_error("Could not decompose the matrix");
    }

    // Find u: ....
    Vector inner = solver.solve(rhs);
    
    u.resize((N+2)*(N+2));
    
    //add boundaries
    for (int i = 0; i < (N+2); ++i) {
        // bottom
        u[i] = g((i+1)*dx, 0);
        // top
        u[i + (N+2)*(N+1)] = g((i+1)*dx, 1);
        // left
        u[i*(N+2)] = g(0, (i+1)*dx);
        // right
        u[i*(N+2) +N+1] = g(1, (i+1)*dx);
    }
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            u[i*(N+2)+1+j] = inner[i*N+j];
        }
    }
    
    
}
//----------------solveEnd----------------


double F(double x, double y) {
     return (1 + 8*M_PI*M_PI)*sin(2*M_PI*x)*cos(2*M_PI*y);
}

double G(double x, double y) {
     return sin(2*M_PI*x);
}



int main(int, char**) {
    Vector u;
    poissonSolve(u, F, 100, G);
    writeToFile("u_fd.txt", u);
}
