#pragma once
#include <Eigen/Core>
#include "load_vector.hpp"

//----------------AssembleVectorBegin----------------
//! Assemble the load vector into the full right hand side
//! for the linear system
//!
//! @param[out] F will at the end contain the RHS values for each vertex.
//! @param[in] vertices a list of triangle vertices
//! @param[in] triangles a list of triangles
//! @param[in] f the RHS function f.
void assembleLoadVector(Eigen::VectorXd& F,
                        const Eigen::MatrixXd& vertices,
                        const Eigen::MatrixXi& triangles,
                        const std::function<double(double, double)>& f)
{
    const int numberOfElements = triangles.rows();

    F.resize(vertices.rows());
    F.setZero();

    Eigen::VectorXd u;
    Eigen::VectorXi interior_vertex_indices;

    for (int i = 0; i < numberOfElements; ++i) {

        Eigen::Vector3d local_load;
        Eigen::Vector2d point_a = vertices.block<1, 2>(triangles(i, 0), 0);
        Eigen::Vector2d point_b = vertices.block<1, 2>(triangles(i, 1), 0);
        Eigen::Vector2d point_c = vertices.block<1, 2>(triangles(i, 2), 0);
        computeLoadVector(local_load, point_a, point_b, point_c, f);

        for (int j = 0; j < 3; ++j) {
            F(triangles(i, j)) += local_load(j);
        }
    }

}
//----------------AssembleVectorEnd----------------
