#pragma once
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <vector>

#include "stiffness_matrix.hpp"

//! Sparse Matrix type. Makes using this type easier.
typedef Eigen::SparseMatrix<double> SparseMatrix;

//! Used for filling the sparse matrix.
typedef Eigen::Triplet<double> Triplet;

//----------------AssembleMatrixBegin----------------
//! Assemble the stiffness matrix
//! for the linear system
//!
//! @param[out] A will at the end contain the Galerkin matrix
//! @param[in] vertices a list of triangle vertices
//! @param[in] triangles a list of triangles
template<class Matrix>
void assembleStiffnessMatrix(Matrix& A, const Eigen::MatrixXd& vertices,
                             const Eigen::MatrixXi& triangles)
{

    const int numberOfElements = triangles.rows();
    A.resize(vertices.rows(), vertices.rows());

    std::vector<Triplet> triplets;

    triplets.reserve(numberOfElements * 3 * 3);

    for (int i = 0; i < numberOfElements; ++i) {
        Eigen::Matrix3d local_stiffness;
        Eigen::Vector2d point_a = vertices.block<1, 2>(triangles(i, 0), 0);
        Eigen::Vector2d point_b = vertices.block<1, 2>(triangles(i, 1), 0);
        Eigen::Vector2d point_c = vertices.block<1, 2>(triangles(i, 2), 0);
        computeStiffnessMatrix(local_stiffness, point_a, point_b, point_c);

        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                triplets.push_back(Triplet(triangles(i, j),triangles(i, k), local_stiffness(j, k)));
            }
        }
    }

    A.setFromTriplets(triplets.begin(), triplets.end());
}
//----------------AssembleMatrixEnd----------------
